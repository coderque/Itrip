package cn.witque.service;

import cn.witque.pojo.ItripUser;

import java.text.ParseException;

public interface TokenSevice {

    String createToken(String agent, ItripUser user);

    void saveToken(String token,ItripUser user);

    void deleteToken(String token);

    boolean validate(String agent,String token);

    ItripUser readToken(String token);

    String replaceToken(String agent,String token) throws ParseException;
}
