package cn.witque.service;

import cn.witque.pojo.ItripUser;
import cn.witque.pojo.dto.Dto;
import com.github.qcloudsms.httpclient.HTTPException;

import java.io.IOException;

public interface UserService {



    Dto registUser(ItripUser record);

    Dto sendMail(String Address);

    ItripUser Login(String userName,String passWord);

    Dto AvtiveUser(String userCode,String activeCode);


    Dto sendPhoneMsg(String PhoneNum) throws HTTPException, IOException;

    boolean isRegist(String userName);

}
