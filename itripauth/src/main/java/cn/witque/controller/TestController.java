package cn.witque.controller;

import cn.witque.pojo.ItripUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @author jyk
 */
@Controller
@ApiModel
@ResponseBody

public class TestController {

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private SimpleMailMessage simpleMailMessage;
    @Resource
    private MailSender mailSender;

    @RequestMapping(value = "goHome",method = RequestMethod.GET)
    @ApiOperation(value = "goHome",produces = "application/json",notes = "结果为json")
    public ItripUser goHome(String userName, HttpServletRequest request){
        String msg = "false";
        ItripUser itripUser = new ItripUser();
        itripUser.setBaidu(msg);
        redisTemplate.opsForHash().delete("user",ItripUser.class);

        String agent = request.getHeader("user-agent");
        System.out.println(agent);

        return itripUser;
    }


    @RequestMapping(value = "mailSend",method = RequestMethod.POST)
    public ItripUser mailSend(){
        ItripUser itripUser = new ItripUser();
        itripUser.setUserName("topcodecompany@163.com");
        String code = UUID.randomUUID().toString();
        itripUser.setUserCode(code);
        simpleMailMessage.setTo(itripUser.getUserName());
        simpleMailMessage.setText("你的激活码是："+code);
        mailSender.send(simpleMailMessage);
        return itripUser;

    }

    @RequestMapping(value = "ng",method = RequestMethod.GET)
    public void ng(){
        System.out.println("ng");
    }

}
