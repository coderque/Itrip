package cn.witque.service.impl;

import cn.witque.dao.ItripAreaDicMapper;
import cn.witque.dao.ItripHotelMapper;
import cn.witque.dao.ItripImageMapper;
import cn.witque.dao.ItripLabelDicMapper;
import cn.witque.pojo.ItripAreaDic;
import cn.witque.pojo.ItripHotelWithBLOBs;
import cn.witque.pojo.ItripLabelDic;
import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.HotelVideoVo;
import cn.witque.pojo.vo.hotel.ImageVo;
import cn.witque.service.HotelService;
import cn.witque.utils.DtoUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author jyk
 * @date 2018年08月27日15:56:54
 */
@Service
public class HotelServiceImpl implements HotelService {

    @Resource
    private ItripHotelMapper itripHotelMapper;

    @Resource
    private ItripAreaDicMapper itripAreaDicMapper;

    @Resource
    private ItripLabelDicMapper itripLabelDicMapper;

    @Resource
    private ItripImageMapper itripImageMapper;


    @Override
    public List<ItripAreaDic> getItripAreaDicListByMap(Map map) {
        return itripAreaDicMapper.getItripAreaDicListByMap(map);
    }

    @Override
    public List<ItripLabelDic> getItripLabelDicListByMap(Map map) {
        return itripLabelDicMapper.getItripLabelDicListByMap(map);
    }

    @Override
    public ItripHotelWithBLOBs selectByPrimaryKey(Long id) {
        return itripHotelMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ItripLabelDic> getHotelFeatureByHotelId(Long id) {
        return itripHotelMapper.getHotelFeatureByHotelId(id);
    }

    @Override
    public List<ImageVo> getImageByMap(Map<String, String> map) {
        return itripImageMapper.getImageByMap(map);
    }

    @Override
    public Dto getVideoDescByHotelId(Long id) {
        HotelVideoVo hotelVideoVo = new HotelVideoVo();
        List<ItripAreaDic> areaList = itripHotelMapper.getHotelAreaByHotelId(id);
        List<ItripLabelDic> labelList = itripHotelMapper.getHotelLabelByHotelId(id);

        List<String> temp1 = new ArrayList<>();
        for (ItripAreaDic areaDic:areaList){
            temp1.add(areaDic.getName());
        }
        hotelVideoVo.setTradingAreaNameList(temp1);

        List<String> temp2 = new ArrayList<>();
        for (ItripLabelDic labelDic:labelList) {
            temp2.add(labelDic.getName());
        }
        hotelVideoVo.setHotelFeatureList(temp2);


        hotelVideoVo.setHotelName(itripHotelMapper.selectByPrimaryKey(id).getHotelName());



        return DtoUtil.getSuccess("获取酒店视频文字描述成功",hotelVideoVo);
    }


}
