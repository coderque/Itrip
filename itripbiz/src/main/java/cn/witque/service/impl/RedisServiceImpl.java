package cn.witque.service.impl;

import cn.witque.pojo.ItripUser;
import cn.witque.service.RedisService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RedisServiceImpl implements RedisService {

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public boolean isexist(String key) {
        try {
        if (redisTemplate.hasKey(key)){
            return true;
        }
        }catch (Exception e){
            return false;
        }

        return false;
    }

    @Override
    public ItripUser getUserToken(String token) {
        ItripUser user = (ItripUser) redisTemplate.opsForValue().get(token);
        return user;
    }


}
