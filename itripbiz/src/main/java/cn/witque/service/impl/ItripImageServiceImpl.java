package cn.witque.service.impl;

import cn.witque.dao.ItripImageMapper;
import cn.witque.pojo.ItripImage;
import cn.witque.service.ItripImageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ItripImageServiceImpl implements ItripImageService {

    @Resource
    private ItripImageMapper itripImageMapper;

    @Override
    public boolean insert(ItripImage record) {
        record.setCreationDate(new Date());
        if (itripImageMapper.insert(record)>0){
            return true;
        }
        return false;
    }
}
