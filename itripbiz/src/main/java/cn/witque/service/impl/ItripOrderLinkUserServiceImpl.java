package cn.witque.service.impl;

import cn.witque.dao.ItripOrderLinkUserMapper;
import cn.witque.pojo.ItripOrderLinkUser;
import cn.witque.service.ItripOrderLinkUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class ItripOrderLinkUserServiceImpl implements ItripOrderLinkUserService {

    @Resource
    private ItripOrderLinkUserMapper itripOrderLinkUserMapper;

    //根据订单Id查
    @Override
    public List<ItripOrderLinkUser> selectByPrimaryKey(@Param("orderId")Long orderId) {
        return itripOrderLinkUserMapper.selectByPrimaryKey(orderId);
    }


}
