package cn.witque.service.impl;

import cn.witque.dao.ItripHotelOrderMapper;
import cn.witque.dao.ItripHotelTempStoreMapper;
import cn.witque.pojo.ItripHotelTempStore;
import cn.witque.pojo.vo.order.AddHotelOrderVO;
import cn.witque.pojo.vo.order.QueryRoomInventoryVo;
import cn.witque.pojo.vo.order.StoreVo;
import cn.witque.pojo.vo.order.ValiDateRoomStoreVO;
import cn.witque.service.ItripHotelTempStoreService;
import cn.witque.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ItripHotelTempStoreServiceImpl implements ItripHotelTempStoreService {

    @Resource
    private ItripHotelTempStoreMapper itripHotelTempStoreMapper;

    @Override
    public List<StoreVo> queryRoomInventory(ValiDateRoomStoreVO valiDateRoomStoreVO) {
        itripHotelTempStoreMapper.selectQuery(valiDateRoomStoreVO);
        return itripHotelTempStoreMapper.queryRoomInventory(valiDateRoomStoreVO);
    }

    @Override
    public ItripHotelTempStore selectByPrimaryKey(Long id) {
        return itripHotelTempStoreMapper.selectByPrimaryKey(id);
    }


    @Override
    public boolean isHasStore(AddHotelOrderVO addHotelOrderVO) {
        Integer count = addHotelOrderVO.getCount();
        ValiDateRoomStoreVO valiDateRoomStoreVO = new ValiDateRoomStoreVO();
        valiDateRoomStoreVO.setCheckInDate(addHotelOrderVO.getCheckInDate());
        valiDateRoomStoreVO.setCheckOutDate(addHotelOrderVO.getCheckOutDate());
        valiDateRoomStoreVO.setCount(addHotelOrderVO.getCount());
        valiDateRoomStoreVO.setHotelId(addHotelOrderVO.getHotelId());
        valiDateRoomStoreVO.setRoomId(addHotelOrderVO.getRoomId());
        itripHotelTempStoreMapper.selectQuery(valiDateRoomStoreVO);

        List<StoreVo> list = itripHotelTempStoreMapper.queryRoomInventory(valiDateRoomStoreVO);
        if (EmptyUtil.isEmpty(list)){
            return false;
        }

        for (StoreVo st: list) {
            if (st.getStore() < count){
                return false;
            }
        }
        return true;
    }
}
