package cn.witque.service.impl;

import cn.witque.dao.ItripLabelDicMapper;
import cn.witque.pojo.vo.ItripLabelDicVO;
import cn.witque.service.ItripLabelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ItripLabelServiceImpl implements ItripLabelService {

    @Resource
    private ItripLabelDicMapper itripLabelDicMapper;

    @Override
    public List<ItripLabelDicVO> getItripLabelDicByParentId(Long parentId) {
        return itripLabelDicMapper.getItripLabelDicByParentId(parentId);
    }
}
