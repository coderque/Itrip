package cn.witque.service.impl;

import cn.witque.dao.ItripUserLinkUserMapper;
import cn.witque.pojo.ItripUserLinkUser;
import cn.witque.service.ItripUserLinkUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ItripUserLinkUserServiceImpl implements ItripUserLinkUserService {

    @Resource
    private ItripUserLinkUserMapper itripUserLinkUserMapper;


    @Override
    public List<ItripUserLinkUser> findByUserId(Long userId,String linkUserName) {
        return itripUserLinkUserMapper.findByUserId(userId,linkUserName);
    }

    @Override
    public ItripUserLinkUser selectById(Long id) {
        return itripUserLinkUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean insert(ItripUserLinkUser record) {
        int num=itripUserLinkUserMapper.insert(record);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        int num=itripUserLinkUserMapper.deleteByPrimaryKey(id);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean update(ItripUserLinkUser record) {
        int num=itripUserLinkUserMapper.update(record);
        if (num>0){
            return true;
        }
        return false;
    }
}
