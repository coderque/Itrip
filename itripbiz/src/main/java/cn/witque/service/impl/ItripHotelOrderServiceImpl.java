package cn.witque.service.impl;

import cn.witque.dao.ItripHotelOrderMapper;
import cn.witque.dao.ItripOrderLinkUserMapper;
import cn.witque.pojo.ItripHotelOrder;
import cn.witque.pojo.ItripOrderLinkUser;
import cn.witque.pojo.ItripUserLinkUser;
import cn.witque.pojo.vo.order.GetPersonalOrderRoomInfoVo;
import cn.witque.service.ItripHotelOrderService;
import cn.witque.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItripHotelOrderServiceImpl implements ItripHotelOrderService {

    @Resource
    private ItripHotelOrderMapper itripHotelOrderMapper;

    @Resource
    private ItripOrderLinkUserMapper itripOrderLinkUserMapper;

    //查询订单列表
    @Override
    public List<ItripHotelOrder> queryOrderList(ItripHotelOrder itripHotelOrder) {
        return itripHotelOrderMapper.queryOrderList(itripHotelOrder);
    }

    @Override
    public int orderCount(ItripHotelOrder itripHotelOrder) {
        return itripHotelOrderMapper.orderCount(itripHotelOrder);
    }


    //查询订单详细信息
    @Override
    public ItripHotelOrder queryOrderDetails(Long id) {
        return itripHotelOrderMapper.queryOrderDetails(id);
    }

    //删除订单
    @Override
    public int deleteByPrimaryKey(Long id) {
        return itripHotelOrderMapper.deleteByPrimaryKey(id);
    }


    //酒店预订
    @Override
    public int insertSelective(ItripHotelOrder record) {
        return itripHotelOrderMapper.insertSelective(record);
    }


    //查询个人订单详情有关的房屋信息  By：itrip_hotel_order  id
    @Override
    public GetPersonalOrderRoomInfoVo getPersonalOrderRoomInfoVo(Long id) {
        return itripHotelOrderMapper.getPersonalOrderRoomInfoVo(id);
    }



    @Override
    public ItripHotelOrder selectByPrimaryKey(Long id) {
        return itripHotelOrderMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean updateOrderStatus(Long id) {
        int num=itripHotelOrderMapper.updateOrderStatus(id);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public Map<String, String> addItripOrder(ItripHotelOrder itripHotelOrder, List<ItripUserLinkUser> userLinkUsers) {
        Long orderId = null;
        if (EmptyUtil.isEmpty(itripHotelOrder.getId())){
            itripHotelOrder.setCreationDate(new Date());
            if (itripHotelOrderMapper.insertSelective(itripHotelOrder) > 0){
                Map<String,String> map = new HashMap<>();
                map.put("orderNo",itripHotelOrder.getOrderNo());
              orderId =  itripHotelOrderMapper.findOrderIdByOrderNo(map);

                for (ItripUserLinkUser linkUser:userLinkUsers) {
                    ItripOrderLinkUser itripOrderLinkUser = new ItripOrderLinkUser();
                    itripOrderLinkUser.setOrderId(orderId);
                    itripOrderLinkUser.setLinkUserId(linkUser.getId());
                    itripOrderLinkUser.setLinkUserName(linkUser.getLinkUserName());
                    itripOrderLinkUser.setCreatedBy(itripHotelOrder.getCreatedBy());
                    itripOrderLinkUser.setCreationDate(new Date());
                    itripOrderLinkUserMapper.insertSelective(itripOrderLinkUser);
                }
            }
        }

        Map<String, String> map = new HashMap<>();
        map.put("id",orderId.toString());
        map.put("orderNo",itripHotelOrder.getOrderNo());

        return map;
    }



    @Override
    public int updateByPrimaryKeySelective(Map<String,Object> map) {
        return itripHotelOrderMapper.updateByPrimaryKeySelective(map);
    }

    @Override
    public boolean flushFail() {
        return itripHotelOrderMapper.flushFailOrderStatus() > 0;
    }

    @Override
    public boolean flushSuccess() {
        return itripHotelOrderMapper.flushFailOrderStatus() >0;
    }


}
