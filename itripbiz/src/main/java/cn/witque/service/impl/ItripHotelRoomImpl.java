package cn.witque.service.impl;

import cn.witque.dao.ItripHotelOrderMapper;
import cn.witque.dao.ItripHotelRoomMapper;
import cn.witque.pojo.ItripHotelRoom;
import cn.witque.service.ItripHotelRoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ItripHotelRoomImpl implements ItripHotelRoomService {
    @Resource
    private ItripHotelRoomMapper itripHotelRoomMapper;
    @Override
    public ItripHotelRoom selectByPrimaryKey(Long id) {
        return itripHotelRoomMapper.selectByPrimaryKey(id);
    }
}
