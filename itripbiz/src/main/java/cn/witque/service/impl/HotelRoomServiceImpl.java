package cn.witque.service.impl;

import cn.witque.dao.ItripHotelRoomMapper;
import cn.witque.pojo.ItripHotelRoom;
import cn.witque.pojo.vo.room.SearchHotelRoomVO;
import cn.witque.service.HotelRoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jyk
 */
@Service
public class HotelRoomServiceImpl implements HotelRoomService {

    @Resource
    private ItripHotelRoomMapper itripHotelRoomMapper;

    @Override
    public List<ItripHotelRoom> getItripHotelRoomListBySearchVo(SearchHotelRoomVO searchHotelRoomVO) {
        return itripHotelRoomMapper.getItripHotelRoomListBySearchVo(searchHotelRoomVO);
    }

    @Override
    public ItripHotelRoom getRoomMsg(Long id) {
        return itripHotelRoomMapper.selectByPrimaryKey(id);
    }


}
