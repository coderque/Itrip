package cn.witque.service.impl;

import cn.witque.dao.ItripCommentMapper;
import cn.witque.pojo.vo.comment.CommentListVO;
import cn.witque.pojo.vo.comment.CommentScoreVO;
import cn.witque.dao.ItripHotelOrderMapper;
import cn.witque.dao.ItripImageMapper;
import cn.witque.pojo.ItripComment;
import cn.witque.pojo.ItripImage;
import cn.witque.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


import java.util.Map;

/**
 * @author jyk
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private ItripHotelOrderMapper itripHotelOrderMapper;

    @Resource
    private ItripImageMapper itripImageMapper;

    @Resource
    private ItripCommentMapper itripCommentMapper;



    @Override
    public Integer getCommentCountByMap(Map<String, Object> map) {
        return itripCommentMapper.getCommentCountByMap(map);
    }


    @Override
    public List<CommentListVO> getCommentListByMap(Map<String, Object> map) {
        return itripCommentMapper.getCommentListByMap(map);
    }


    @Override
    public CommentScoreVO getAllScore(Long hotelId) {
        return itripCommentMapper.getAllScore(hotelId);
    }




    @Override
    public boolean insert(ItripComment record, List<ItripImage> itripImages) {
        float score=record.getFacilitiesScore()+record.getHygieneScore()+record.getPositionScore()+record.getServiceScore();
        record.setScore(Math.round(score/4));
        int num=itripCommentMapper.insert(record);
        Long id=0L;
        if (num>0){
            id=record.getId();
            if (itripImages!=null && itripImages.size()>0 ){
                for (ItripImage itripImage:itripImages){
                    itripImage.setTargetId(id);
                    itripImageMapper.insert(itripImage);
                }
            }
            itripHotelOrderMapper.updateOrderStatus(record.getOrderId());
            return true;
        }
        return false;
    }

}
