package cn.witque.service;

import cn.witque.pojo.ItripAreaDic;
import cn.witque.pojo.ItripHotelWithBLOBs;
import cn.witque.pojo.ItripLabelDic;
import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.ImageVo;

import java.util.List;
import java.util.Map;

public interface HotelService {



    List<ItripAreaDic> getItripAreaDicListByMap(Map map);

    List<ItripLabelDic> getItripLabelDicListByMap(Map map);

    ItripHotelWithBLOBs selectByPrimaryKey(Long id);

    List<ItripLabelDic> getHotelFeatureByHotelId(Long id);

    List<ImageVo> getImageByMap(Map<String,String> map);

    Dto getVideoDescByHotelId(Long id);

}
