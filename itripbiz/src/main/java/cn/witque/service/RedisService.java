package cn.witque.service;

import cn.witque.pojo.ItripUser;

public interface RedisService {

    boolean isexist(String token);

    ItripUser getUserToken(String token);
}
