package cn.witque.service;

import cn.witque.pojo.vo.ItripLabelDicVO;

import java.util.List;

public interface ItripLabelService {

    List<ItripLabelDicVO> getItripLabelDicByParentId(Long parentId);
}
