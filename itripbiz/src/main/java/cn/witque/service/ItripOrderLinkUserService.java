package cn.witque.service;

import cn.witque.pojo.ItripOrderLinkUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItripOrderLinkUserService {

    //根据订单Id查
    List<ItripOrderLinkUser> selectByPrimaryKey(@Param("orderId") Long orderId);
}