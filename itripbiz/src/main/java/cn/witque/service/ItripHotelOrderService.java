package cn.witque.service;

import cn.witque.pojo.ItripHotelOrder;
import cn.witque.pojo.ItripUserLinkUser;
import cn.witque.pojo.vo.order.GetPersonalOrderRoomInfoVo;

import java.util.List;
import java.util.Map;

public interface ItripHotelOrderService {

    //查询订单列表
    List<ItripHotelOrder> queryOrderList(ItripHotelOrder itripHotelOrder);

    //订单列表分页
    int  orderCount(ItripHotelOrder itripHotelOrder);

    //查询订单详细信息
    ItripHotelOrder queryOrderDetails(Long id);

    //删除订单
    int deleteByPrimaryKey(Long id);

    //酒店预订
    int insertSelective(ItripHotelOrder record);

    //查询个人订单详情有关的房屋信息  By：itrip_hotel_order  id
    GetPersonalOrderRoomInfoVo getPersonalOrderRoomInfoVo(Long id);

    //查询订单详细信息
    ItripHotelOrder selectByPrimaryKey(Long id);

    //修改订单状态
    boolean updateOrderStatus(Long id);


    Map<String,String> addItripOrder(ItripHotelOrder itripHotelOrder, List<ItripUserLinkUser> userLinkUsers);

    //线下支付
    int updateByPrimaryKeySelective(Map<String,Object> map);


    boolean flushFail();

    boolean flushSuccess();
}
