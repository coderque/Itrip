package cn.witque.service;

import cn.witque.pojo.vo.comment.CommentListVO;
import cn.witque.pojo.vo.comment.CommentScoreVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import cn.witque.pojo.ItripComment;
import cn.witque.pojo.ItripImage;

/**
 * @author jyk
 */
public interface CommentService {


    Integer getCommentCountByMap(Map<String,Object> map);

    List<CommentListVO> getCommentListByMap(Map<String,Object> map);

    CommentScoreVO getAllScore(@Param("hotelId") Long hotelId);


    boolean insert(ItripComment record, List<ItripImage> itripImages);
}
