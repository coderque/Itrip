package cn.witque.service;

import cn.witque.pojo.ItripHotelTempStore;
import cn.witque.pojo.vo.order.AddHotelOrderVO;
import cn.witque.pojo.vo.order.QueryRoomInventoryVo;
import cn.witque.pojo.vo.order.StoreVo;
import cn.witque.pojo.vo.order.ValiDateRoomStoreVO;

import java.util.List;

public interface ItripHotelTempStoreService {

    //查询房间库存
    List<StoreVo> queryRoomInventory(ValiDateRoomStoreVO valiDateRoomStoreVO);

    /**
     * 根据Id查
     * @param id
     * @return
     */
    ItripHotelTempStore selectByPrimaryKey(Long id);


    boolean isHasStore(AddHotelOrderVO addHotelOrderVO);
}
