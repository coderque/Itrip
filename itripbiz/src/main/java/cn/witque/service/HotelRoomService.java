package cn.witque.service;

import cn.witque.pojo.ItripHotelRoom;
import cn.witque.pojo.vo.room.SearchHotelRoomVO;

import java.util.List;

public interface HotelRoomService {

    List<ItripHotelRoom> getItripHotelRoomListBySearchVo(SearchHotelRoomVO searchHotelRoomVO);

    ItripHotelRoom getRoomMsg(Long id);
}
