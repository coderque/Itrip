package cn.witque.service;

import cn.witque.pojo.ItripUserLinkUser;

import java.util.List;

public interface ItripUserLinkUserService {

    List<ItripUserLinkUser> findByUserId( Long userId,String linkUserName);

    ItripUserLinkUser selectById(Long id);

    boolean insert(ItripUserLinkUser record);

    boolean deleteById(Long id);

    boolean update(ItripUserLinkUser record);

}
