package cn.witque.controller;

import cn.witque.pojo.ItripAreaDic;
import cn.witque.pojo.ItripHotelWithBLOBs;
import cn.witque.pojo.ItripLabelDic;
import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.ImageVo;
import cn.witque.service.HotelService;
import cn.witque.service.RedisService;
import cn.witque.utils.DtoUtil;
import cn.witque.utils.EmptyUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author jyk
 */
@Controller
@RequestMapping("/api/hotel")
public class HotelController {


    @Resource
    private HotelService hotelService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private RedisService redisService;


    /**
     * 根据国家类型查询热门城市1.国内 2.国外
     */
    @RequestMapping(value = "/queryhotcity/{type}", method = RequestMethod.GET)
    @ResponseBody
    public Dto queryhotcity(@PathVariable Integer type){

        if (EmptyUtil.isEmpty(type)){
            DtoUtil.getFailed("类型不能为空","123");
        }
        Map param = new HashMap();
        param.put("isHot", 1);
        param.put("isChina", type);

        String redisKey = "hotel:hotHotelList:"+type;
        List<ItripAreaDic> list =null;
        if (redisService.isexist(redisKey)){
            list = (List<ItripAreaDic>) redisTemplate.opsForValue().get(redisKey);
        }else {
            list = hotelService.getItripAreaDicListByMap(param);
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        if (list.size() < 1){
            return DtoUtil.getFailed("为找到任何符合条件的酒店！","124");
        }

        return DtoUtil.getSuccess(list);
    }


    /**
     * 展示酒店特色列表
     * @return
     */
    @RequestMapping(value = "/queryhotelfeature",method = RequestMethod.GET)
    @ResponseBody
    public Dto queryhotelfeature(){
        Map param = new HashMap();
        param.put("parentId", 16);
        List<ItripLabelDic> list = null;

        String redisKey = "hotel:hotelFeatureList";
        if (redisService.isexist(redisKey)){
            list = (List<ItripLabelDic>) redisTemplate.opsForValue().get(redisKey);
        }else {
            list = hotelService.getItripLabelDicListByMap(param);
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        if (list.size() < 1){
            return DtoUtil.getFailed("为找到任何符合条件的酒店特色！","125");
        }
        return DtoUtil.getSuccess(list);
    }


    /**
     * 根据城市ID查出商圈
     * @param cityId
     * @return
     */
    @RequestMapping(value = "/querytradearea/{cityId}",method = RequestMethod.GET)
    @ResponseBody
    public Dto queryTradeArea(@PathVariable Long cityId) {
        if (EmptyUtil.isEmpty(cityId)){
            DtoUtil.getFailed("城市id不能为空","124");
        }
        Map param = new HashMap();
        param.put("isTradingArea", 1);
        param.put("parent", cityId);
        List<ItripAreaDic> list =null;
        String redisKey = "hotel:cityAreaList:"+cityId;
        if (redisService.isexist(redisKey)){
            list = (List<ItripAreaDic>) redisTemplate.opsForValue().get(redisKey);
        }else {
            list = hotelService.getItripAreaDicListByMap(param);
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        if (list.size() < 1){
            return DtoUtil.getFailed("为找到任何符合条件的区域信息！","125");
        }

        return DtoUtil.getSuccess(list);
    }



    @RequestMapping(value = "/queryhoteldetails/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Dto queryHotelDetails(@PathVariable Long id){
        if (EmptyUtil.isEmpty(id)){
            DtoUtil.getFailed("酒店id不能为空","125");
        }
        List<Object> list = new ArrayList<>();

        String redisKey = "hotel:hotelDetailsList:"+id;
        if (redisService.isexist(redisKey)){
            list = (List<Object>) redisTemplate.opsForValue().get(redisKey);
        }else {

            ItripHotelWithBLOBs itripHotel = hotelService.selectByPrimaryKey(id);
            List<ItripLabelDic> itripLabelDicList = hotelService.getHotelFeatureByHotelId(id);
            Map<String,Object> map = new HashMap();
            map.put("name","酒店介绍");
            map.put("description",itripHotel.getDetails());

            list.add(map);

            for (ItripLabelDic lb:itripLabelDicList) {
                Map<String,Object> labels = new HashMap();
                labels.put("name",lb.getName());
                labels.put("description",lb.getDescription());
                list.add(labels);
            }
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        return DtoUtil.getSuccess(list);
    }

    /**
     * 根据酒店查询酒店图片
     * @param targetId
     * @return
     */
    @RequestMapping(value = "/getimg/{targetId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Dto getImgByTargetId(@PathVariable String targetId) {

        if (EmptyUtil.isEmpty(targetId)){
            DtoUtil.getFailed("酒店id不能为空","126");
        }
        List<ImageVo> list = new ArrayList<>();
        String redisKey = "hotel:hotelImgGet:"+targetId;
        if (redisService.isexist(redisKey)){
            list = (List<ImageVo>) redisTemplate.opsForValue().get(redisKey);
        }else {
            Map<String, String> param = new HashMap<>();
            param.put("type", "0");
            param.put("targetId", targetId);
            list = hotelService.getImageByMap(param);
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        if (list.size()<1){
            return DtoUtil.getFailed("获取图片列表失败！","127");
        }
        return DtoUtil.getSuccess("获取图片列表成功！",list);
    }

    /**
     * 根据酒店id查询酒店设施
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryhotelfacilities/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Dto queryHotelFacilities(@PathVariable Long id) {
        if (EmptyUtil.isEmpty(id)){
            DtoUtil.getFailed("酒店id不能为空","126");
        }
        ItripHotelWithBLOBs itripHotel = null;
        String redisKey = "hotel:hotelFacilities:"+id;
        if (redisService.isexist(redisKey)){
            itripHotel = (ItripHotelWithBLOBs) redisTemplate.opsForValue().get(redisKey);
        }else {
             itripHotel = hotelService.selectByPrimaryKey(id);
            redisTemplate.opsForValue().set(redisKey,itripHotel,2,TimeUnit.HOURS);
        }

        if (EmptyUtil.isEmpty(itripHotel)){
            DtoUtil.getFailed("未找到酒店设施！","128");
        }

        return DtoUtil.getSuccess("获得酒店特色成功！",itripHotel.getFacilities());
    }


    /**
     * 根据酒店id查询酒店政策
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryhotelpolicy/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Dto queryHotelPolicy(@PathVariable Long id){
        if (EmptyUtil.isEmpty(id)){
            DtoUtil.getFailed("酒店id不能为空","126");
        }

        ItripHotelWithBLOBs itripHotel =null;

        String redisKey = "hotel:hotelPolicy:"+id;
        if (redisService.isexist(redisKey)){
            itripHotel = (ItripHotelWithBLOBs) redisTemplate.opsForValue().get(redisKey);
        }else {
            itripHotel = hotelService.selectByPrimaryKey(id);
            redisTemplate.opsForValue().set(redisKey,itripHotel,2,TimeUnit.HOURS);
        }

        if (EmptyUtil.isEmpty(itripHotel)){
            DtoUtil.getFailed("未找到酒店政策！","127");
        }
        return DtoUtil.getSuccess("获得酒店政策成功！",itripHotel.getHotelPolicy());
    }


    /**
     * 获取酒店视频区域的文字描述
     * @param id
     * @return
     */
    @RequestMapping(value = "/getvideodesc/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Dto getVideoDescByHotelId(@PathVariable Long id) {
        if (EmptyUtil.isEmpty(id)){
            DtoUtil.getFailed("酒店id不能为空", "130");
        }
//        DtoUtil.getFailed("获取酒店视频文字描述失败", "129");
        return hotelService.getVideoDescByHotelId(id);
    }



}
