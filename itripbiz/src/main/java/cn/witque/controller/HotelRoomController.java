package cn.witque.controller;

import cn.witque.pojo.ItripHotelRoom;
import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.ItripLabelDicVO;
import cn.witque.pojo.vo.hotel.ImageVo;
import cn.witque.pojo.vo.room.SearchHotelRoomVO;
import cn.witque.service.HotelRoomService;
import cn.witque.service.HotelService;
import cn.witque.service.ItripLabelService;
import cn.witque.service.RedisService;
import cn.witque.utils.DateUtil;
import cn.witque.utils.DtoUtil;
import cn.witque.utils.EmptyUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/api/hotelroom")
public class HotelRoomController {

    @Resource
    private ItripLabelService itripLabelService;

    @Resource
    private HotelRoomService hotelRoomService;

    @Resource
    private HotelService hotelService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private RedisService redisService;

    @RequestMapping(value = "/queryhotelroombed", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Dto queryHotelRoomBed() {

            List<ItripLabelDicVO> list = null;
            String redisKey = "room:hotelRoomBed";
            if (redisService.isexist(redisKey)){
                list = (List<ItripLabelDicVO>) redisTemplate.opsForValue().get(redisKey);
            }else {
                list = itripLabelService.getItripLabelDicByParentId(new Long(1));
                redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
            }

           if (EmptyUtil.isEmpty(list) || list.size()<1){
               return DtoUtil.getFailed("获取床型失败", "131");
           }

           return DtoUtil.getSuccess("获取床型成功", list);

    }

    /**
     * 根据酒店条件查询房间的信息
     * @param searchHotelRoomVO
     * @return
     */
    @RequestMapping(value = "/queryhotelroombyhotel", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Dto queryHotelRoomByHotel(@RequestBody SearchHotelRoomVO searchHotelRoomVO) {
        if (EmptyUtil.isEmpty(searchHotelRoomVO.getHotelId())){
            return DtoUtil.getFailed("酒店id不能空了","132");
        }

        if (EmptyUtil.isEmpty(searchHotelRoomVO.getStartDate()) || EmptyUtil.isEmpty(searchHotelRoomVO.getEndDate())) {
            return DtoUtil.getFailed("入住和退房时间也不能空了","133");
        }

        if (!EmptyUtil.isEmpty(searchHotelRoomVO.getStartDate()) && !EmptyUtil.isEmpty(searchHotelRoomVO.getEndDate())) {
            if (searchHotelRoomVO.getStartDate().getTime() > searchHotelRoomVO.getEndDate().getTime()) {
                return DtoUtil.getFailed("入住时间不能大于退房时间", "134");
            }
        }

        searchHotelRoomVO.setTimeList(DateUtil.getBetweenDates(searchHotelRoomVO.getStartDate(),searchHotelRoomVO.getEndDate()));




        try{
            List<List<ItripHotelRoom>> hotelRoomList  = new ArrayList();
            String redisKey = "room:hotelRoomByHotelId:"+searchHotelRoomVO.getHotelId();
            if (redisService.isexist(redisKey)){
                hotelRoomList = (List<List<ItripHotelRoom>>) redisTemplate.opsForValue().get(redisKey);
            }else {
                List<ItripHotelRoom> list = hotelRoomService.getItripHotelRoomListBySearchVo(searchHotelRoomVO);
                for (ItripHotelRoom hotelRoom: list) {
                    List<ItripHotelRoom> tempList = new ArrayList<ItripHotelRoom>();
                    tempList.add(hotelRoom);
                    hotelRoomList.add(tempList);
                }
                redisTemplate.opsForValue().set(redisKey,hotelRoomList,2,TimeUnit.HOURS);
            }

           return DtoUtil.getSuccess("获取房间信息成功！",hotelRoomList);

        }catch (Exception e){
            e.printStackTrace();
            return DtoUtil.getFailed("获取房间信息失败！","133");
        }
    }

    /**
     * 获得房型图片
     * @param targetId
     * @return
     */
    @RequestMapping(value = "/getimg/{targetId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Dto getImgByTargetId(@PathVariable String targetId) {

        if (EmptyUtil.isEmpty(targetId)){
            DtoUtil.getFailed("酒店id不能为空","126");
        }
        Map<String, String> param = new HashMap<>();
        param.put("type", "1");
        param.put("targetId", targetId);

        String redisKey = "room:roomTypeImg:"+targetId;
        List<ImageVo> list = null;
        if (redisService.isexist(redisKey)){
            list = (List<ImageVo>) redisTemplate.opsForValue().get(redisKey);
        }else {
            list = hotelService.getImageByMap(param);
            redisTemplate.opsForValue().set(redisKey,list,2,TimeUnit.HOURS);
        }

        if (list.size()<1){
            return DtoUtil.getFailed("获取房型图片失败！","127");
        }
        return DtoUtil.getSuccess("获取房型图片列表成功！",list);
    }


}
