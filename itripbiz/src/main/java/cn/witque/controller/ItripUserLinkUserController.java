package cn.witque.controller;


import cn.witque.pojo.ItripUser;
import cn.witque.pojo.ItripUserLinkUser;
import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.ItripUserLinkUserVo;
import cn.witque.service.ItripUserLinkUserService;
import cn.witque.utils.DtoUtil;
import io.swagger.annotations.ApiModel;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@ApiModel
@RequestMapping("/api/userinfo")
public class ItripUserLinkUserController {

    @Resource
    private ItripUserLinkUserService itripUserLinkUserService;

    @Resource
    private RedisTemplate redisTemplate;
    /**
     * 查询旅客信息列表
     * */
    @RequestMapping(value = "/queryuserlinkuser",method = RequestMethod.POST)
    @ResponseBody
    public Dto<ItripUserLinkUser> findByUserId(HttpServletRequest request,
                                               @RequestBody Map<String,String> map){
        String linkUserName = map.get("linkUserName");
        String token = request.getHeader("token");
        ItripUser itripUser = (ItripUser) redisTemplate.opsForValue().get(token);
        Long userId = itripUser.getId();

        List<ItripUserLinkUser> list=itripUserLinkUserService.findByUserId(userId,linkUserName);
        if (list != null){
            return DtoUtil.getSuccess(list);
        }else {
            return DtoUtil.getFailed("获取信息失败","199");
        }
    }


    /**
     * 新增旅客信息
     * */
    @RequestMapping(value = "/adduserlinkuser",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Dto<Object> insert(@RequestBody ItripUserLinkUserVo itripUserLinkUserVo,HttpServletRequest request){
        String token = request.getHeader("token");
        ItripUser itripUser = (ItripUser) redisTemplate.opsForValue().get(token);
        Long userId=itripUser.getId();
        if (itripUserLinkUserVo!=null && itripUser!=null) {
            ItripUserLinkUser user = new ItripUserLinkUser();
            user.setId(itripUserLinkUserVo.getId());
            user.setLinkUserName(itripUserLinkUserVo.getLinkUserName());
            user.setLinkIdCard(itripUserLinkUserVo.getLinkIdCard());
            user.setLinkPhone(itripUserLinkUserVo.getLinkPhone());
            user.setUserId(new Long(userId).intValue());
            user.setCreationDate(new Date());
            user.setCreatedBy(userId);
            if (itripUserLinkUserService.insert(user)) {
                return DtoUtil.getSuccess("增加信息成功");
            } else {
                return DtoUtil.getFailed("增加信息失败", "197");
            }
        }else if (itripUserLinkUserVo==null && itripUser !=null){
            return DtoUtil.getFailed("提交内容不能为空","196");
        }else {
            return DtoUtil.getFailed("用户名失效，请重新登录","195");
        }
    }

    /**
     * 修改旅客信息
     * */
    @RequestMapping(value = "/modifyuserlinkuser",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Dto<Object> modify(@RequestBody ItripUserLinkUserVo itripUserLinkUserVo,HttpServletRequest request){
        String token = request.getHeader("token");
        ItripUser itripUser = (ItripUser) redisTemplate.opsForValue().get(token);
        Long userId=itripUser.getId();
        if (itripUserLinkUserVo!=null && itripUser!=null) {
            ItripUserLinkUser user = new ItripUserLinkUser();
            user.setId(itripUserLinkUserVo.getId());
            user.setLinkUserName(itripUserLinkUserVo.getLinkUserName());
            user.setLinkIdCard(itripUserLinkUserVo.getLinkIdCard());
            user.setLinkPhone(itripUserLinkUserVo.getLinkPhone());
            user.setUserId(new Long(userId).intValue());
            user.setCreationDate(new Date());
            user.setCreatedBy(userId);

            if (itripUserLinkUserService.update(user)) {
                return DtoUtil.getSuccess("修改信息成功");
            } else {
                return DtoUtil.getFailed("修改信息失败", "194");
            }
        }else if (itripUserLinkUserVo==null && itripUser !=null){
            return DtoUtil.getFailed("提交内容不能为空","196");
        }else {
            return DtoUtil.getFailed("用户名失效，请重新登录","195");
        }
    }




    /**
     * 删除旅客信息
     * @param ids
     * */
    @RequestMapping(value = "/deluserlinkuser",method = RequestMethod.GET)
    @ResponseBody
    public Dto<Object> delete(@RequestParam String ids,HttpServletRequest request){
        String token = request.getHeader("token");
        ItripUser itripUser = (ItripUser) redisTemplate.opsForValue().get(token);
        if(itripUser!=null) {
            if (itripUserLinkUserService.deleteById(Long.valueOf(ids))) {
                return DtoUtil.getSuccess("删除信息成功");
            } else {
                return DtoUtil.getFailed("删除信息失败", "198");
            }
        }else {
            return DtoUtil.getFailed("用户名失效，请重新登录","195");
        }
    }
}
