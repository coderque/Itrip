package cn.witque.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ItripUserLinkUserVo {
    //主键
    private Long id;
    //常用联系人姓名
    private String linkUserName;
    //证件号码
    private String linkIdCard;
    //常用联系人电话
    private String linkPhone;
    //用户id
    private Integer userId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date creationDate;

    private Long createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkUserName() {
        return linkUserName;
    }

    public void setLinkUserName(String linkUserName) {
        this.linkUserName = linkUserName;
    }

    public String getLinkIdCard() {
        return linkIdCard;
    }

    public void setLinkIdCard(String linkIdCard) {
        this.linkIdCard = linkIdCard;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ItripUserLinkUserVo(Long id, String linkUserName, String linkIdCard, String linkPhone, Integer userId, Long createdBy) {
        this.id = id;
        this.linkUserName = linkUserName;
        this.linkIdCard = linkIdCard;
        this.linkPhone = linkPhone;
        this.userId = userId;
        this.createdBy = createdBy;
    }

    public ItripUserLinkUserVo() {
    }
}
