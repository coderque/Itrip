package cn.witque.pojo.vo.hotel;

import java.io.Serializable;

public class ImageVo implements Serializable {
    //页面图片展现顺序
    private Integer position;
    //图片的URL访问路径
    private String imgUrl;


    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
