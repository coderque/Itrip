package cn.witque.pojo.vo.hotel;

import java.io.Serializable;
import java.util.List;

/**
 * @author jyk
 */
public class HotelVideoVo implements Serializable {

    //酒店名称
    private String hotelName;

    //酒店所属商圈
    private List<String> tradingAreaNameList;

    //酒店特色
    private List<String> hotelFeatureList;


    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public List<String> getTradingAreaNameList() {
        return tradingAreaNameList;
    }

    public void setTradingAreaNameList(List<String> tradingAreaNameList) {
        this.tradingAreaNameList = tradingAreaNameList;
    }

    public List<String> getHotelFeatureList() {
        return hotelFeatureList;
    }

    public void setHotelFeatureList(List<String> hotelFeatureList) {
        this.hotelFeatureList = hotelFeatureList;
    }
}
