package cn.witque.pojo.vo.comment;

import java.io.Serializable;
import java.util.Date;

/**
 * 返回前端-点评列表VO
 *
 */
public class CommentListVO implements Serializable {
	//评论id
	private Long id;
	//发表评论的用户的姓名
	private String userCode;
	//酒店的星级
	private Integer hotelLevel;
	//入住时间
	private Date checkInDate;
	//房型名称
	private String roomTitle;
	//出游类型中文名称
	private String tripModeName;
	//评论内容
	private String content;
	//评论发表时间
	private Date creationDate;
	//综合评分
	private Integer score;
	//是否有评论图片
	private Integer isHavingImg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTripModeName() {
		return tripModeName;
	}

	public void setTripModeName(String tripModeName) {
		this.tripModeName = tripModeName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getIsHavingImg() {
		return isHavingImg;
	}

	public void setIsHavingImg(Integer isHavingImg) {
		this.isHavingImg = isHavingImg;
	}

	public Integer getHotelLevel() {
		return hotelLevel;
	}

	public void setHotelLevel(Integer hotelLevel) {
		this.hotelLevel = hotelLevel;
	}

	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getRoomTitle() {
		return roomTitle;
	}
	public void setRoomTitle(String roomTitle) {
		this.roomTitle = roomTitle;
	}
}
