package cn.witque.pojo.vo.order;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

/**
 * 酒店预订VO
 *
 */
public class HotelBookingVo {

    //入住时间(预订时间)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Timestamp checkInDate;

    //预订天数
    private Integer bookingDays;

    //入住人名称
    private String linkUserName;

    //联系手机号
    private String noticePhone;

    //是否需要发票（0：不需要 1：需要）
    private Integer isNeedInvoice;

    //发票类型（0：个人 1：公司）
    private Integer invoiceType;

    //发票抬头
    private String invoiceHead;

    //特殊需求
    private String specialRequirement;

    public Timestamp getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Timestamp checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Integer getBookingDays() {
        return bookingDays;
    }

    public void setBookingDays(Integer bookingDays) {
        this.bookingDays = bookingDays;
    }

    public String getLinkUserName() {
        return linkUserName;
    }

    public void setLinkUserName(String linkUserName) {
        this.linkUserName = linkUserName;
    }

    public String getNoticePhone() {
        return noticePhone;
    }

    public void setNoticePhone(String noticePhone) {
        this.noticePhone = noticePhone;
    }

    public Integer getIsNeedInvoice() {
        return isNeedInvoice;
    }

    public void setIsNeedInvoice(Integer isNeedInvoice) {
        this.isNeedInvoice = isNeedInvoice;
    }

    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceHead() {
        return invoiceHead;
    }

    public void setInvoiceHead(String invoiceHead) {
        this.invoiceHead = invoiceHead;
    }

    public String getSpecialRequirement() {
        return specialRequirement;
    }

    public void setSpecialRequirement(String specialRequirement) {
        this.specialRequirement = specialRequirement;
    }
}
