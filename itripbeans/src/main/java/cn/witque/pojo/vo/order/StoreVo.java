package cn.witque.pojo.vo.order;

import java.io.Serializable;
import java.util.Date;


/**
 * 返回的库存Vo
 */
public class StoreVo implements Serializable {

    private Long roomId;

    private Date date;

    private Integer store;


    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }
}
