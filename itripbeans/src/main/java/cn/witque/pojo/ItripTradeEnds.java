package cn.witque.pojo;

import java.io.Serializable;

public class ItripTradeEnds implements Serializable {
    //订单ID
    private Long id;
    //订单编号(注意非支付宝交易编号tradeNo)
    private String orderNo;
    //标识字段(默认0：未处理；1：处理中)
    private Boolean flag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}