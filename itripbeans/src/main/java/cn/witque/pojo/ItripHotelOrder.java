package cn.witque.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ItripHotelOrder implements Serializable {

    //主键
    private Long id;

    //用户id
    private Long userId;

    //订单类型(0:旅游产品 1:酒店产品 2：机票产品)
    private Integer orderType;

    //订单号
    private String orderNo;

    //交易编号
    private String tradeNo;

    //冗余字段 酒店id
    private Long hotelId;

    //冗余字段 酒店名称
    private String hotelName;

    //房间id
    private Long roomId;

    //房间数量
    private Integer count;

    //预订天数
    private Integer bookingDays;

    //入住时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date checkInDate;

    //退房时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date checkOutDate;

    //订单状态（0：待支付 1:已取消 2:支付成功 3:已消费 4：已点评）
    private Integer orderStatus;

    //支付金额
    private BigDecimal payAmount;

    //支付方式:1:支付宝 2:微信 3:到店付
    private Integer payType;

    //联系手机号
    private String noticePhone;

    //联系邮箱
    private String noticeEmail;

    //是否需要发票（0：不需要 1：需要）
    private Integer isNeedInvoice;

    //发票类型（0：个人 1：公司）
    private Integer invoiceType;

    //发票抬头
    private String invoiceHead;

    //入住人名称
    private String linkUserName;

    //0:WEB端 1:手机端 2:其他客户端
    private Integer bookType;

    //预定时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date creationDate;

    private Long createdBy;
    //支付完成时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date modifyDate;

    private Long modifiedBy;

    //特殊需求
    private String specialRequirement;

    private Integer pageIndex;

    private Integer pageSize;

    List<ItripOrderLinkUser> itripOrderLinkUserList;

    public List<ItripOrderLinkUser> getItripOrderLinkUserList() {
        return itripOrderLinkUserList;
    }

    public void setItripOrderLinkUserList(List<ItripOrderLinkUser> itripOrderLinkUserList) {
        this.itripOrderLinkUserList = itripOrderLinkUserList;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getBookingDays() {
        return bookingDays;
    }

    public void setBookingDays(Integer bookingDays) {
        this.bookingDays = bookingDays;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getNoticePhone() {
        return noticePhone;
    }

    public void setNoticePhone(String noticePhone) {
        this.noticePhone = noticePhone;
    }

    public String getNoticeEmail() {
        return noticeEmail;
    }

    public void setNoticeEmail(String noticeEmail) {
        this.noticeEmail = noticeEmail;
    }

    public Integer getIsNeedInvoice() {
        return isNeedInvoice;
    }

    public void setIsNeedInvoice(Integer isNeedInvoice) {
        this.isNeedInvoice = isNeedInvoice;
    }

    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceHead() {
        return invoiceHead;
    }

    public void setInvoiceHead(String invoiceHead) {
        this.invoiceHead = invoiceHead;
    }

    public String getLinkUserName() {
        return linkUserName;
    }

    public void setLinkUserName(String linkUserName) {
        this.linkUserName = linkUserName;
    }

    public Integer getBookType() {
        return bookType;
    }

    public void setBookType(Integer bookType) {
        this.bookType = bookType;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getSpecialRequirement() {
        return specialRequirement;
    }

    public void setSpecialRequirement(String specialRequirement) {
        this.specialRequirement = specialRequirement;
    }


    //额外添加的时间字段(用于查询时间区间的字段)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date endDate;

    private String processNode;
    private Integer roomPayType;

    public Integer getRoomPayType() {
        return roomPayType;
    }

    public void setRoomPayType(Integer roomPayType) {
        this.roomPayType = roomPayType;
    }

    public String getProcessNode() {
        return processNode;
    }

    public void setProcessNode(String processNode) {
        this.processNode = processNode;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


}