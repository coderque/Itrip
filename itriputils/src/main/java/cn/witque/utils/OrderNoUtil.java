package cn.witque.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 生成订单号
 */
public class OrderNoUtil {


    public static String randomOrderNumber(String usercode) {

        SimpleDateFormat simpleDateFormat;

        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        Date date = new Date();

        String str = simpleDateFormat.format(date);

        return  "D1000001"+str+Md5Util.getMd5(usercode,6) ;
    }




    public static Date checkOutDates(Date checkInDate,Integer bookingDays) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(checkInDate);
        calendar.add(Calendar.DAY_OF_MONTH, +bookingDays);//+1今天的时间加一天
        Date date = calendar.getTime();
        return date;
    }

}
