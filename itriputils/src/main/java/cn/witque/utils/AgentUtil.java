package cn.witque.utils;

/**
 * @author jyk
 */
public class AgentUtil {

    public static String getUserMachine(String agent){
        String userMachine = "Unknown";
        if (agent.indexOf("Macintosh") != -1){
            userMachine = "Mac";
        }else if (agent.indexOf("iPhone") != -1){
            userMachine = "IPhone";
        }else if (agent.indexOf("Android") != -1){
            userMachine = "Android";
        }else if (agent.indexOf("Windows") != -1){
            userMachine = "Windows";
        }
        return  userMachine;
    }
}
