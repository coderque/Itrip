package cn.witque.utils;

public class SystemConfig {
    /**
     * 文件上传linux路径
     */
    private String fileUploadPathLinux;
    /**
     * 上传文件访问URL，通过properties文件进行配置
     */
    private String visitImgUrlString;
    /**
     * 生成订单的机器码，通过properties文件进行配置
     */
    private String machineCode;




    public String getVisitImgUrlString() {
        return visitImgUrlString;
    }

    public void setVisitImgUrlString(String visitImgUrlString) {
        this.visitImgUrlString = visitImgUrlString;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getFileUploadPathLinux() {
        return fileUploadPathLinux;
    }

    public void setFileUploadPathLinux(String fileUploadPathLinux) {
        this.fileUploadPathLinux = fileUploadPathLinux;
    }
}
