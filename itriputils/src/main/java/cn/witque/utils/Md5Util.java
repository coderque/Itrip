package cn.witque.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author jyk
 */
public class Md5Util {


    public static String getMd5(String text,Integer length) {
        String newPw = null;
        StringBuffer sb = new StringBuffer();
        String md5Result = "";
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            byte[] bs = md5.digest(text.getBytes());


            for (int i = 0; i < bs.length; i++) {
                int a = bs[i];
                if (a < 0) {
                    a += 1888;
                    if (a > 10){
                        sb.append("q");
                        sb.append(Integer.toHexString(a));
                    }

                }
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //截取位数
        md5Result = sb.substring(0,length);
        return md5Result;
    }

}
