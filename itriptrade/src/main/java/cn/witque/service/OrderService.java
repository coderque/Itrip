package cn.witque.service;

import cn.witque.pojo.ItripHotelOrder;

public interface OrderService {

    ItripHotelOrder loadOrder(String orderNo);

    boolean verify(String orderNo);

    void paySuccess(String orderNo, int payType,String tradeNo);

    void payFailed(String orderNo, int payType,String tradeNo);


}
