package cn.witque.service;

import cn.witque.dao.ItripHotelOrderMapper;
import cn.witque.pojo.ItripHotelOrder;
import cn.witque.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author jyk
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Resource
    private ItripHotelOrderMapper itripHotelOrderMapper;


    @Override
    public ItripHotelOrder loadOrder(String orderNo) {
        ItripHotelOrder itripHotelOrder = new ItripHotelOrder();
        itripHotelOrder.setOrderNo(orderNo);
        ItripHotelOrder order = itripHotelOrderMapper.selectByOrderNo(itripHotelOrder);
        if (!EmptyUtil.isEmpty(itripHotelOrder)){
          return order;
        }
        return null;
    }

    @Override
    public boolean verify(String orderNo) {
        ItripHotelOrder itripHotelOrder = this.loadOrder(orderNo);

        if (itripHotelOrder.getOrderStatus().equals("2") || null == itripHotelOrder.getOrderStatus()){
            return true;
        }
        return false;
    }





    @Override
    public void paySuccess(String orderNo, int payType, String tradeNo) {
        ItripHotelOrder itripHotelOrder = this.loadOrder(orderNo);
        itripHotelOrder.setOrderStatus(2);
        itripHotelOrder.setPayType(payType);
        itripHotelOrder.setTradeNo(tradeNo);
        itripHotelOrderMapper.finisOrder(itripHotelOrder);
    }



    @Override
    public void payFailed(String orderNo, int payType, String tradeNo) {
        ItripHotelOrder itripHotelOrder = this.loadOrder(orderNo);
        itripHotelOrder.setOrderStatus(1);
        itripHotelOrder.setPayType(payType);
        itripHotelOrder.setTradeNo(tradeNo);
        itripHotelOrderMapper.finisOrder(itripHotelOrder);
    }
}
