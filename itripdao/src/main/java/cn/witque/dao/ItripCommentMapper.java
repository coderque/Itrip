package cn.witque.dao;

import cn.witque.pojo.ItripComment;
import cn.witque.pojo.vo.comment.CommentListVO;
import cn.witque.pojo.vo.comment.CommentScoreVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ItripCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripComment record);

    int insertSelective(ItripComment record);

    ItripComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripComment record);

    int updateByPrimaryKeyWithBLOBs(ItripComment record);

    int updateByPrimaryKey(ItripComment record);


    /**
     * 为了方便复用 所以用了map
     * @param map
     * @return
     */
    Integer getCommentCountByMap(Map<String,Object> map);

    /**
     * 根据map查询评论集合
     * @param map
     * @return
     */
    List<CommentListVO> getCommentListByMap(Map<String,Object> map);

    CommentScoreVO getAllScore(@Param("hotelId") Long hotelId);

}