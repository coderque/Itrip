package cn.witque.dao;

import cn.witque.pojo.ItripHotelOrder;
import cn.witque.pojo.vo.order.GetPersonalOrderRoomInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ItripHotelOrderMapper {
    //删除订单
    int deleteByPrimaryKey(Long id);

    int insert(ItripHotelOrder record);

    //酒店预订
    int insertSelective(ItripHotelOrder record);

    ItripHotelOrder selectByPrimaryKey(Long id);

    //线下支付
    int updateByPrimaryKeySelective(Map<String,Object> map);

    int updateByPrimaryKeyWithBLOBs(ItripHotelOrder record);

    int updateByPrimaryKey(ItripHotelOrder record);

    //查询订单列表
    List<ItripHotelOrder> queryOrderList(ItripHotelOrder itripHotelOrder);

    //订单列表分页
    int  orderCount(ItripHotelOrder itripHotelOrder);

    //查询订单详细信息
    ItripHotelOrder queryOrderDetails(Long id);

    //查询个人订单详情有关的房屋信息  By：itrip_hotel_order  id
    GetPersonalOrderRoomInfoVo getPersonalOrderRoomInfoVo(Long id);

    //修改订单状态
    int updateOrderStatus(@Param("id")Long id);

    Long findOrderIdByOrderNo(Map<String,String> orderMap);


    ItripHotelOrder selectByOrderNo(ItripHotelOrder itripHotelOrder);

    int finisOrder(ItripHotelOrder itripHotelOrder);


    Integer flushFailOrderStatus();


    Integer flushSuccessOrderStatus();
}