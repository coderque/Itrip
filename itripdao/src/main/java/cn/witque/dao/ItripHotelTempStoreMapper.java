package cn.witque.dao;

import cn.witque.pojo.ItripHotelTempStore;
import cn.witque.pojo.vo.order.QueryRoomInventoryVo;
import cn.witque.pojo.vo.order.StoreVo;
import cn.witque.pojo.vo.order.ValiDateRoomStoreVO;

import java.util.List;
import java.util.Map;

public interface ItripHotelTempStoreMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripHotelTempStore record);

    int insertSelective(ItripHotelTempStore record);

    //根据Id查
    ItripHotelTempStore selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripHotelTempStore record);

    int updateByPrimaryKey(ItripHotelTempStore record);

    //查询房间库存
   List<StoreVo> queryRoomInventory(ValiDateRoomStoreVO valiDateRoomStoreVO);

   Integer selectQuery(ValiDateRoomStoreVO valiDateRoomStoreVO);



}