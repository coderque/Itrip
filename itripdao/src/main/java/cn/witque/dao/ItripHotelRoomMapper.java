package cn.witque.dao;

import cn.witque.pojo.ItripHotelRoom;
import cn.witque.pojo.vo.room.SearchHotelRoomVO;

import java.util.List;

public interface ItripHotelRoomMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripHotelRoom record);

    int insertSelective(ItripHotelRoom record);

    //根据id查询
    ItripHotelRoom selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripHotelRoom record);

    int updateByPrimaryKey(ItripHotelRoom record);

    List<ItripHotelRoom>getItripHotelRoomListBySearchVo(SearchHotelRoomVO searchHotelRoomVO);




}