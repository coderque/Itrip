package cn.witque.dao;

import cn.witque.pojo.ItripOrderLinkUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItripOrderLinkUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripOrderLinkUser record);

    int insertSelective(ItripOrderLinkUser record);

    //根据订单Id查
    List<ItripOrderLinkUser> selectByPrimaryKey(@Param("orderId") Long orderId);

    int updateByPrimaryKeySelective(ItripOrderLinkUser record);

    int updateByPrimaryKey(ItripOrderLinkUser record);
}