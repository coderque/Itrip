package cn.witque.dao;

import cn.witque.pojo.ItripAreaDic;

import java.util.List;
import java.util.Map;

public interface ItripAreaDicMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripAreaDic record);

    int insertSelective(ItripAreaDic record);

    ItripAreaDic selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripAreaDic record);

    int updateByPrimaryKey(ItripAreaDic record);

    List<ItripAreaDic> getItripAreaDicListByMap(Map map);

}