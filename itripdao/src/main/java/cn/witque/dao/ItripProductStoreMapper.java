package cn.witque.dao;

import cn.witque.pojo.ItripProductStore;

public interface ItripProductStoreMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripProductStore record);

    int insertSelective(ItripProductStore record);

    ItripProductStore selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripProductStore record);

    int updateByPrimaryKey(ItripProductStore record);
}