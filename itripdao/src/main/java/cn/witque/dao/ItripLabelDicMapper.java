package cn.witque.dao;

import cn.witque.pojo.ItripLabelDic;
import cn.witque.pojo.vo.ItripLabelDicVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ItripLabelDicMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripLabelDic record);

    int insertSelective(ItripLabelDic record);



    int updateByPrimaryKeySelective(ItripLabelDic record);

    int updateByPrimaryKey(ItripLabelDic record);


    List<ItripLabelDic> getItripLabelDicListByMap(Map map);

    ItripLabelDic selectByPrimaryKey(Long id);


    /**
     * 从字典中查出所有床型
     * @param parentId
     * @return
     */
    List<ItripLabelDicVO> getItripLabelDicByParentId(@Param(value = "parentId")Long parentId);


}