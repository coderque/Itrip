package cn.witque.dao;

import cn.witque.pojo.ItripImage;
import cn.witque.pojo.vo.hotel.ImageVo;

import java.util.List;
import java.util.Map;

public interface ItripImageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripImage record);

    int insertSelective(ItripImage record);

    ItripImage selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripImage record);

    int updateByPrimaryKey(ItripImage record);

    List<ImageVo> getImageByMap(Map<String,String> map);
}