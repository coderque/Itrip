package cn.witque.dao;

import cn.witque.pojo.ItripUserLinkUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItripUserLinkUserMapper {
    //删除旅客信息
    int deleteByPrimaryKey(Long id);

    //新增旅客信息
    int insert(ItripUserLinkUser record);

    int insertSelective(ItripUserLinkUser record);

    ItripUserLinkUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ItripUserLinkUser record);

    int updateByPrimaryKey(ItripUserLinkUser record);

    //查询旅客信息
    List<ItripUserLinkUser> findByUserId(@Param("userId") Long userId,
                                         @Param("linkUserName")String linkUserName);

    //修改旅客信息
    int update(ItripUserLinkUser itripUserLinkUser);
}