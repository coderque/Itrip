package cn.witque.dao;

import cn.witque.pojo.ItripAreaDic;
import cn.witque.pojo.ItripHotel;
import cn.witque.pojo.ItripHotelWithBLOBs;
import cn.witque.pojo.ItripLabelDic;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ItripHotelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripHotelWithBLOBs record);

    int insertSelective(ItripHotelWithBLOBs record);

    int updateByPrimaryKeySelective(ItripHotelWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ItripHotelWithBLOBs record);

    int updateByPrimaryKey(ItripHotel record);


    List<ItripHotelWithBLOBs> searchHotelByChoice();

    List<ItripHotelWithBLOBs> getItripHotelListByMap(Map map);

    ItripHotelWithBLOBs selectByPrimaryKey(Long id);

    List<ItripLabelDic> getHotelFeatureByHotelId(@Param(value = "id")Long id);



    List<ItripAreaDic> getHotelAreaByHotelId(@Param(value = "id") Long id);

    List<ItripLabelDic> getHotelLabelByHotelId(@Param(value = "id") Long id);
}