package cn.witque.dao;

import cn.witque.pojo.ItripUser;

public interface ItripUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ItripUser record);

    ItripUser selectByPrimaryKey(Long id);

    int updateByPrimaryKey(ItripUser record);




    //注册用户
    int insertSelective(ItripUser record);

    //根据用户名查状态
    ItripUser selectActiveByUserCode(String UserCode);


    //根据用户名去激活用户 || 或者修改用户
    int updateByPrimaryKeySelective(ItripUser record);

}