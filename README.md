# Itrip

#### 项目介绍
爱旅行项目：
分为四个模块,共计18个功能<br>
模块分别为：auth、biz、trade、search<br>
组长：雀雀<br>
组员：老曹、丐丐<br>

#### 软件架构
软件架构说明


#### 功能划分
项目进度表：<a href="https://docs.qq.com/sheet/BqI21X2yZIht1J9evM19pJCl1JK6sK2sbi8u3PmpRL1ZW21f1" target="_blank">点击查看详细项目进度表</a>

订单支付<br>
1. 丐丐：<br>
酒店点评<br>
查询常用旅客列表<br>
新增常用旅客信息<br>
修改常用旅客信息<br>
删除常用旅客信息<br>


2.老曹：
酒店预订 <br>
订单查询<br>
订单详情查询<br>
订单删除<br>

3. 雀雀：
登录、注销、注册<br>
查询酒店列表<br>
查询酒店房间<br>
查询酒店详情<br>
酒店点评查询<br> 

#### 工具共享
<a href="http://blog.witque.cn/238/">Solr和IK分词器下载</a>


#### 参与贡献

1. 完成后修改此处
2. 
3. 
4. 

