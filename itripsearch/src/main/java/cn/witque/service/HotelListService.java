package cn.witque.service;

import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.SearchHotelVo;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;

/**
 * @author jyk
 */
public interface HotelListService {

    Dto searchItripHotelPage(SearchHotelVo searchHotelVo);

    Dto searchItripHotelListByHotCity(Integer cityId,Integer count);

}
