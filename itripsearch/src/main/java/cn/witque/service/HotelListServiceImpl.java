package cn.witque.service;

import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.SearchHotelVo;
import cn.witque.utils.DtoUtil;
import cn.witque.utils.EmptyUtil;
import cn.witque.utils.Page;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author jyk
 */
@Service
public class HotelListServiceImpl implements HotelListService {


    @Resource
    private HttpSolrClient httpSolrClient;

    @Resource
    private XMLResponseParser xmlResponseParser;




    @Override
    public Dto searchItripHotelPage(SearchHotelVo searchHotelVo) {

        SolrQuery solrQuery  = new SolrQuery();
        httpSolrClient.setParser(xmlResponseParser);
        httpSolrClient.setConnectionTimeout(3000);

        StringBuilder sbQuery = new StringBuilder();

        int temp = 0;
        //以下都是不为空的情况
        if (!EmptyUtil.isEmpty(searchHotelVo)) {
            //城市是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getDestination())) {
                sbQuery.append("destination:" + searchHotelVo.getDestination());
                temp = 1;
            }

            //酒店等级是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getHotelLevel())) {
                solrQuery.addFilterQuery("hotelLevel:" + searchHotelVo.getHotelLevel());
            }

            //关键词是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getKeywords())) {
                String keyword = "";
                try {
                    keyword = new String(searchHotelVo.getKeywords().getBytes("iso8859-1"),"utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return DtoUtil.getFailed("编码转换出错了！","122");
                }
                if (temp == 1) {
                    sbQuery.append("and keyword:" + keyword);
                } else {
                    sbQuery.append("keyword:" + keyword);
                }
            }

            //酒店特色是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getFeatureIds())) {
                StringBuilder featureSb = new StringBuilder("(");
                int flag = 0;
                String features[] = searchHotelVo.getFeatureIds().split(",");
                for (String featureId : features) {
                    if (flag == 0) {
                        featureSb.append(" featureIds:*," + featureId + ",*");
                    } else {
                        featureSb.append(" OR featureIds:*," + featureId + ",*");
                    }
                    flag++;
                }
                featureSb.append(")");
                solrQuery.addFilterQuery(featureSb.toString());
            }


            //商圈id是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getTradeAreaIds())) {
                StringBuffer tradeSb = new StringBuffer("(");
                int flag = 0;
                String trades[] = searchHotelVo.getTradeAreaIds().split(",");
                for (String tradeAreaId : trades) {
                    if (flag == 0) {
                        tradeSb.append(" tradingAreaIds:*," + tradeAreaId + ",*");
                    } else {
                        tradeSb.append(" OR tradingAreaIds:*," + tradeAreaId + ",*");
                    }
                    flag++;
                }
                tradeSb.append(")");
                solrQuery.addFilterQuery(tradeSb.toString());

            }

            //最高价是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getMaxPrice())) {
                solrQuery.addFilterQuery("minPrice:" + "[* TO " + searchHotelVo.getMaxPrice() + "]");
            }


            //最底价是否为空
            if (!EmptyUtil.isEmpty(searchHotelVo.getMinPrice())) {
                solrQuery.addFilterQuery("minPrice:" + "[" + searchHotelVo.getMinPrice() + " TO *]");
            }

            //排序方式
            if (!EmptyUtil.isEmpty(searchHotelVo.getAscSort())) {
                solrQuery.addSort(searchHotelVo.getAscSort(), SolrQuery.ORDER.asc);
            }

            if (!EmptyUtil.isEmpty(searchHotelVo.getDescSort())) {
                solrQuery.addSort(searchHotelVo.getDescSort(), SolrQuery.ORDER.desc);
            }
        }

        if (!EmptyUtil.isEmpty(sbQuery.toString())) {
            solrQuery.setQuery(sbQuery.toString());
        }

        Page page;

        try {
            QueryResponse response = httpSolrClient.query(solrQuery);
            SolrDocumentList docs = response.getResults();

            if (EmptyUtil.isEmpty(searchHotelVo.getPageNo())){
                searchHotelVo.setPageNo(1);
            }

            if (EmptyUtil.isEmpty(searchHotelVo.getPageSize())){
                searchHotelVo.setPageSize(5);
            }

            page = new Page(searchHotelVo.getPageNo(),searchHotelVo.getPageSize(),docs.size());
            solrQuery.setStart(page.getBeginPos());
            solrQuery.setRows(page.getPageSize());
            QueryResponse responsePage = httpSolrClient.query(solrQuery);
            List list = responsePage.getResults();
            page.setRows(list);

        } catch (SolrServerException e) {
            e.printStackTrace();
            return DtoUtil.getFailed("solr数据库执行出现问题，请联系管理员","121");
        } catch (IOException e) {
            e.printStackTrace();
            return DtoUtil.getFailed("solr数据库执行出现问题，请联系管理员","121");
        }

        return DtoUtil.getSuccess(page);
    }

    @Override
    public Dto searchItripHotelListByHotCity(Integer cityId, Integer count){
        SolrQuery solrQuery = new SolrQuery("*:*");

        if (!EmptyUtil.isEmpty(cityId)) {
            solrQuery.addFilterQuery("cityId:" + cityId);
        } else {
            return null;
        }

        solrQuery.setStart(0);
        if(EmptyUtil.isEmpty(count)){
            solrQuery.setRows(5);
        }else {
            solrQuery.setRows(count);
        }

        QueryResponse response = null;
        try {
            response = httpSolrClient.query(solrQuery);
        } catch (SolrServerException e) {
            e.printStackTrace();
            return DtoUtil.getFailed("solr数据库执行出现问题，请联系管理员","121");
        } catch (IOException e) {
            e.printStackTrace();
            return DtoUtil.getFailed("solr数据库执行出现问题，请联系管理员","121");
        }
        List list = response.getResults();
        return DtoUtil.getSuccess(list);
    }


}
