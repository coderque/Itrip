package cn.witque.controller;

import cn.witque.pojo.dto.Dto;
import cn.witque.pojo.vo.hotel.SearchHotelVo;
import cn.witque.service.HotelListService;
import cn.witque.utils.DtoUtil;
import cn.witque.utils.EmptyUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/api/hotellist")
public class SearchHotelController {

    @Resource
    private HotelListService hotelListService;

    /**
     * 分页查询酒店列表
     * @param searchHotelVo
     * @return
     */
    @RequestMapping(value = "/searchItripHotelPage",method = RequestMethod.POST)
    @ResponseBody
    public Dto searchItripHotelPage(@RequestBody SearchHotelVo searchHotelVo){

        return hotelListService.searchItripHotelPage(searchHotelVo);
    }

    /**
     * 根据热门城市查询酒店
     * cityId [必填] 城市id ,count [必填] 数目
     * @return
     */
    @RequestMapping(value = "/searchItripHotelListByHotCity",method = RequestMethod.POST)
    @ResponseBody
    public Dto searchItripHotelListByHotCity(@RequestBody Map<String,Integer> map){

        if (EmptyUtil.isEmpty(map) || EmptyUtil.isEmpty(map.get("cityId"))){
            DtoUtil.getFailed("城市ID不能为空","123");
        }

            return hotelListService.searchItripHotelListByHotCity(map.get("cityId"),map.get("count"));
    }


}
